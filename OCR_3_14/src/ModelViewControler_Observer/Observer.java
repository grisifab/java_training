package ModelViewControler_Observer;

public interface Observer {
	
	public void update(String str);

}
