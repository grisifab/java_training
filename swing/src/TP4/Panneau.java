package TP4;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;
import java.awt.GradientPaint; 



public class Panneau extends JPanel { 
  public void paintComponent(Graphics g){
    //Vous verrez cette phrase chaque fois que la méthode sera invoquée
	  Graphics2D g2d = (Graphics2D)g;
	  GradientPaint gp = new GradientPaint(0,0,Color.yellow,300,300,Color.gray,true);
	  g2d.setPaint(gp);
	  g2d.fillRect(0, 0, 300, 300);
  }               
}