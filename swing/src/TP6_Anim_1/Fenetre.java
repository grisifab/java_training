package TP6_Anim_1;

import java.awt.Dimension; 
import javax.swing.JFrame;
 
public class Fenetre extends JFrame{
  private Panneau pan = new Panneau();

  public Fenetre(){        
    this.setTitle("Animation");
    this.setSize(300, 400);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    this.setContentPane(pan);
    this.setVisible(true);
    go();
  }

  private void go(){
	  int sensX=1,sensY=1;
	  for(;;){
	    int x = pan.getPosX(), y = pan.getPosY();
	    if(x == (pan.getWidth()-50)){sensX = -1;}
	    if(x == 0){sensX = 1;}
	    if(y == (pan.getHeight()-50)){sensY = -1;}
	    if(y == 0){sensY = 1;}
	    x+= sensX;
	    y+= sensY;
	    pan.setPosX(x);
	    pan.setPosY(y);
	    pan.repaint();  
	    try {
	      Thread.sleep(10);
	    } catch (InterruptedException e) {
	      e.printStackTrace();
	    }
	  }
	}
         
}