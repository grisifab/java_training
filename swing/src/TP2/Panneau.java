package TP2;

import java.awt.Graphics;
import javax.swing.JPanel;
 
public class Panneau extends JPanel { 
  public void paintComponent(Graphics g){
    //Vous verrez cette phrase chaque fois que la méthode sera invoquée
    int x1 = this.getWidth()/4;
    int y1 = this.getHeight()/4;
    g.fillOval(x1, y1, x1*2, y1*2);
  }               
}