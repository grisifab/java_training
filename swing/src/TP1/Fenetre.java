package TP1;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;

public class Fenetre extends JFrame {
  public Fenetre(){
    this.setTitle("Ma première fenêtre Java");
    this.setSize(400, 500);
    this.setLocationRelativeTo(null);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);   
    
    JPanel pan = new JPanel();
    pan.setBackground(Color.ORANGE);
    this.setContentPane(pan);
    
    this.setVisible(true);
  }
}