package TP3;

import java.awt.Graphics;
import javax.swing.JPanel;
 
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


public class Panneau extends JPanel { 
  public void paintComponent(Graphics g){
    //Vous verrez cette phrase chaque fois que la méthode sera invoquée
	  try {
	      Image img = ImageIO.read(new File("cible.jpg")); // à la racine du projet
	      g.drawImage(img, 0, 0,this.getWidth(),this.getHeight(), this);
	      //Pour une image de fond
	      //g.drawImage(img, 0, 0, this.getWidth(), this.getHeight(), this);
	    } catch (IOException e) {
	      e.printStackTrace();
	    } 
  }               
}