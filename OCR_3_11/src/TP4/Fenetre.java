package TP4;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;



public class Fenetre extends JFrame {
private JTree arbre;
private DefaultTreeModel model;

public Fenetre(){
  this.setSize(300, 300);
  this.setLocationRelativeTo(null);
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  this.setTitle("Les arbres");
  //On invoque la méthode de construction de notre arbre
  buildTree();
    
  this.setVisible(true);
}

private void buildTree(){
  //Création d'une racine
  DefaultMutableTreeNode racine = new DefaultMutableTreeNode("Racine");

  //Nous allons ajouter des branches et des feuilles à notre racine
  for(int i = 1; i < 12; i++){
    DefaultMutableTreeNode rep = new DefaultMutableTreeNode("Noeud n°"+i);

    //S'il s'agit d'un nombre pair, on rajoute une branche
    if((i%2) == 0){
      //Et une branche en plus ! Une !
      for(int j = 1; j < 5; j++){
        DefaultMutableTreeNode rep2 = new DefaultMutableTreeNode("Fichier enfant n°" + j);
        //Cette fois, on ajoute les feuilles
        for(int k = 1; k < 5; k++)
          rep2.add(new DefaultMutableTreeNode("Sous-fichier enfant n°" + k));
        rep.add(rep2);
      }
    }
    //On ajoute la feuille ou la branche à la racine
    racine.add(rep);
  }
  
//Nous créons notre modèle
  model = new DefaultTreeModel(racine);
  //Et nous allons écouter ce que notre modèle a à nous dire ! 
  model.addTreeModelListener(new TreeModelListener() {
    /**
    * Méthode appelée lorsqu'un noeud a changé
     */
    public void treeNodesChanged(TreeModelEvent evt) {
      System.out.println("Changement dans l'arbre");
      Object[] listNoeuds = evt.getChildren();
      int[] listIndices = evt.getChildIndices();
      for (int i = 0; i < listNoeuds.length; i++) {
        System.out.println("Index " + listIndices[i] + ", nouvelle valeur :" + listNoeuds[i]);
      }
    }

    /**
    * Méthode appelée lorsqu'un noeud est inséré
     */
    public void treeNodesInserted(TreeModelEvent event) {
      System.out.println("Un noeud a été inséré !");            
    }

    /**
    * Méthode appelée lorsqu'un noeud est supprimé
     */
    public void treeNodesRemoved(TreeModelEvent event) {
      System.out.println("Un noeud a été retiré !");
    }

    /**
    * Méthode appelée lorsque la structure d'un noeud a été modifiée
     */
    public void treeStructureChanged(TreeModelEvent event) {
      System.out.println("La structure d'un noeud a changé !");
    }
  });

  //Nous créons, avec notre hiérarchie, un arbre
  arbre = new JTree();
  //Nous passons notre modèle à notre arbre 
  //==> On pouvait aussi passer l'objet TreeModel au constructeur du JTree
  arbre.setModel(model);
  arbre.setRootVisible(false);
  //On rend notre arbre éditable
  arbre.setEditable(true);
  

  //Que nous plaçons sur le ContentPane de notre JFrame à l'aide d'un scroll 
  this.getContentPane().add(new JScrollPane(arbre));
}

public static void main(String[] args){
	 try {
	      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());            
	    }
	    catch (InstantiationException e) {}
	    catch (ClassNotFoundException e) {}
	    catch (UnsupportedLookAndFeelException e) {}
	    catch (IllegalAccessException e) {}
  Fenetre fen = new Fenetre();
}   
}