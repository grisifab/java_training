package TP3;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;



public class Fenetre extends JFrame {
private JTree arbre;
private JTextArea jta;
public Fenetre(){
  this.setSize(400, 300);
  this.setLocationRelativeTo(null);
  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  this.setTitle("Les arbres");
  //On invoque la méthode de construction de notre arbre
  buildTree();
    
  this.setVisible(true);
}

private void buildTree(){
  
	// panneau supplémentaire pour affichage info
	jta = new JTextArea();
	
	
	//Création d'une racine
  DefaultMutableTreeNode racine = new DefaultMutableTreeNode("Racine");

  //Nous allons ajouter des branches et des feuilles à notre racine
  for(int i = 1; i < 12; i++){
    DefaultMutableTreeNode rep = new DefaultMutableTreeNode("Noeud n°"+i);

    //S'il s'agit d'un nombre pair, on rajoute une branche
    if((i%2) == 0){
      //Et une branche en plus ! Une !
      for(int j = 1; j < 5; j++){
        DefaultMutableTreeNode rep2 = new DefaultMutableTreeNode("Fichier enfant n°" + j);
        //Cette fois, on ajoute les feuilles
        for(int k = 1; k < 5; k++)
          rep2.add(new DefaultMutableTreeNode("Sous-fichier enfant n°" + k));
        rep.add(rep2);
      }
    }
    //On ajoute la feuille ou la branche à la racine
    racine.add(rep);
  }
  //Nous créons, avec notre hiérarchie, un arbre
  arbre = new JTree(racine);
  
  arbre.addTreeSelectionListener(new TreeSelectionListener(){

      public void valueChanged(TreeSelectionEvent event) {
        if(arbre.getLastSelectedPathComponent() != null){
          //System.out.println(getAbsolutePath(event.getPath()));
          jta.setText("");
          jta.setText(getAbsolutePath(event.getPath()));
        }
      }
      
      private String getAbsolutePath(TreePath treePath){
    	    String str = "";
    	    //On balaie le contenu de l'objet TreePath
    	    for(Object name : treePath.getPath()){
    	      //Si l'objet a un nom, on l'ajoute au chemin
    	      if(name.toString() != null)
    	        str += name.toString() + " ";
    	    }
    	    return str;
    	  }
    });

  	//split qui contient l'arbo et le panneau d'infos
	JSplitPane split = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,new JScrollPane(arbre),jta);
	split.setDividerLocation(150);
  
 
  this.getContentPane().add(split);
}

public static void main(String[] args){
  Fenetre fen = new Fenetre();
}   
}