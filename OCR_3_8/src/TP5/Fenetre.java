package TP5;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;

public class Fenetre extends JFrame {

	private Panneau pan = new Panneau();
	private JButton bouton = new JButton("Go");
	private JButton bouton2 = new JButton("Stop");
	private JPanel container = new JPanel();
	private JLabel label = new JLabel("Choix de la forme");
	private int compteur = 0;
	private boolean animated = true;
	private boolean backX, backY;
	private int x, y;
	private Thread t;
	private JComboBox combo = new JComboBox();
	private JCheckBox morph = new JCheckBox("Morphing");

	private JMenuBar maBar = new JMenuBar();

	private JMenu menuForm = new JMenu("Formes");
	private JMenu menuMorph = new JMenu("Morphing");
	private JMenu menuAcv = new JMenu("Animation");

	private JRadioButtonMenuItem carre = new JRadioButtonMenuItem("CARRE");
	private JRadioButtonMenuItem triangle = new JRadioButtonMenuItem("TRIANGLE");
	private JRadioButtonMenuItem rond = new JRadioButtonMenuItem("ROND");
	private JRadioButtonMenuItem etoile = new JRadioButtonMenuItem("ETOILE");

	private JRadioButtonMenuItem oui1 = new JRadioButtonMenuItem("Oui");
	private JRadioButtonMenuItem non1 = new JRadioButtonMenuItem("Non");

	private JRadioButtonMenuItem oui2 = new JRadioButtonMenuItem("Oui");
	private JRadioButtonMenuItem non2 = new JRadioButtonMenuItem("Non");

	public Fenetre() {
		this.setTitle("Animation");
		this.setSize(350, 350);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		container.setBackground(Color.white);
		container.setLayout(new BorderLayout());
		container.add(pan, BorderLayout.CENTER);
		bouton.addActionListener(new BoutonListener());
		bouton2.addActionListener(new Bouton2Listener());
		bouton2.setEnabled(false);
		JPanel south = new JPanel();
		south.add(bouton);
		south.add(bouton2);
		container.add(south, BorderLayout.SOUTH);
		combo.addItem("ROND");
		combo.addItem("CARRE");
		combo.addItem("TRIANGLE");
		combo.addItem("ETOILE");
		combo.addActionListener(new FormeListener());
		morph.addActionListener(new MorphListener());

		JPanel top = new JPanel();
		top.add(label);
		top.add(combo);
		top.add(morph);
		container.add(top, BorderLayout.NORTH);

		// --------
		this.menuAcv.add(oui1);
		this.menuAcv.add(non1);
		ButtonGroup bg1 = new ButtonGroup();
		bg1.add(oui1);
		bg1.add(non1);
		non1.setSelected(true);
		this.maBar.add(menuAcv);
		oui1.addActionListener(new BoutonListener());
		non1.addActionListener(new Bouton2Listener());
		menuAcv.setMnemonic('A');
		oui1.setMnemonic('O');
		oui1.setAccelerator(KeyStroke.getKeyStroke('c'));

		this.menuMorph.add(oui2);
		this.menuMorph.add(non2);
		ButtonGroup bg2 = new ButtonGroup();
		bg2.add(oui2);
		bg2.add(non2);
		non2.setSelected(true);
		this.maBar.add(menuMorph);
		non2.addActionListener(new MorphListener0());
		oui2.addActionListener(new MorphListener1());

		this.menuForm.add(carre);
		this.menuForm.add(triangle);
		this.menuForm.add(rond);
		this.menuForm.add(etoile);
		ButtonGroup bg3 = new ButtonGroup();
		bg3.add(carre);
		bg3.add(rond);
		bg3.add(triangle);
		bg3.add(etoile);
		rond.setSelected(true);
		carre.addActionListener(new FormeListener2());
		triangle.addActionListener(new FormeListener2());
		rond.addActionListener(new FormeListener2());
		etoile.addActionListener(new FormeListener2());
		this.maBar.add(menuForm);

		this.setJMenuBar(maBar);

		// --------
		this.setContentPane(container);
		this.setVisible(true);
	}

	private void go() {
		x = pan.getPosX();
		y = pan.getPosY();
		while (this.animated) {

			// Si le mode morphing est activé, on utilise la taille actuelle de
			// la forme
			if (pan.isMorph()) {
				if (x < 1)
					backX = false;
				if (x > pan.getWidth() - pan.getDrawSize())
					backX = true;
				if (y < 1)
					backY = false;
				if (y > pan.getHeight() - pan.getDrawSize())
					backY = true;
			}
			// Sinon, on fait comme d'habitude
			else {
				if (x < 1)
					backX = false;
				if (x > pan.getWidth() - 50)
					backX = true;
				if (y < 1)
					backY = false;
				if (y > pan.getHeight() - 50)
					backY = true;
			}

			if (!backX)
				pan.setPosX(++x);
			else
				pan.setPosX(--x);
			if (!backY)
				pan.setPosY(++y);
			else
				pan.setPosY(--y);
			pan.repaint();
			try {
				Thread.sleep(3);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public class BoutonListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane jop = new JOptionPane();
			int option = jop.showConfirmDialog(null,
					"Voulez-vous lancer l'animation ?",
					"Lancement de l'animation", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (option == JOptionPane.OK_OPTION) {
				animated = true;
				t = new Thread(new PlayAnimation());
				t.start();
				bouton.setEnabled(false);
				bouton2.setEnabled(true);
			}
		}
	}

	class Bouton2Listener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JOptionPane jop = new JOptionPane();
			int option = jop.showConfirmDialog(null,
					"Voulez-vous arrêter l'animation ?",
					"Arrêt de l'animation", JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE);

			if (option != JOptionPane.NO_OPTION
					&& option != JOptionPane.CANCEL_OPTION
					&& option != JOptionPane.CLOSED_OPTION) {
				animated = false;
				bouton.setEnabled(true);
				bouton2.setEnabled(false);
			}
		}
	}

	class PlayAnimation implements Runnable {
		public void run() {
			go();
		}
	}

	class FormeListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			pan.setForme(combo.getSelectedItem().toString());
		}
	}

	class FormeListener2 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.out.println(menuForm.getSelectedObjects());
			pan.setForme(((JRadioButtonMenuItem) e.getSource()).getText());
		}
	}

	class MorphListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			// Si la case est cochée, on active le mode morphing
			if (morph.isSelected())
				pan.setMorph(true);
			// Sinon, on ne fait rien
			else
				pan.setMorph(false);
		}
	}

	class MorphListener1 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			pan.setMorph(true);
		}
	}

	class MorphListener0 implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			pan.setMorph(false);
		}
	}
}