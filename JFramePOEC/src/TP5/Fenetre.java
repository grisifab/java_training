package TP5;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Fenetre extends JFrame{
  //private JPanel pan = new JPanel(); 1ere méthode
  private JButton bouton = new JButton("Mon bouton");

  public Fenetre(){
    this.setTitle("Animation");
    this.setSize(300, 150);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    
    //1ere méthode Ajout du bouton à notre content pane
    //pan.add(bouton);
    //this.setContentPane(pan);
    
    //2eme méthode bouton mieux centré
    this.getContentPane().add(bouton);
    
    this.setVisible(true);
  }       
}
