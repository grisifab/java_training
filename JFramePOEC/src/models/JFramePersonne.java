package models;

//import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
//import com.mysql.cj.protocol.Resultset;
//import net.proteanit.sql.DbUtils;
import services.MyConnection;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
//import java.sql.ResultSet;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Color;

public class JFramePersonne extends JFrame {	
	
	private JPanel contentPane;
	private JTable table;
	private JButton btnInsert;
	private JButton btnUpdate;
	private JButton btnDelete;
	private JTextField textNum;
	private JTextField textNom;
	private JTextField textPrenom;	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFramePersonne frame = new JFramePersonne();
					frame.setVisible(true);
					frame.fillData();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}	/**
	 * Create the frame.
	 */
	public JFramePersonne() {
		setTitle("CRUDE Opération");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		JButton btnSelect = new JButton("Select");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				fillData();
			}});		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(20, 11, 252, 239);
		contentPane.add(scrollPane);		
		table = new JTable();
		scrollPane.setViewportView(table);
		btnSelect.setBounds(305, 227, 89, 23);
		contentPane.add(btnSelect);		
		btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection c = MyConnection.getConnection();
				try {
					String sql = "INSERT INTO `jdbc`.`personne` (`nom`, `prenom`) VALUES ('"+textNom.getText()+"', '"+textPrenom.getText()+"');";
					PreparedStatement pst = c.prepareStatement(sql);
					int rs = pst.executeUpdate();
					// Reset Text Fields
					textNum.setText("");
					textNom.setText("");
					textPrenom.setText("");
					JOptionPane.showMessageDialog(null, "Inséré !!!!");
					fillData();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});		btnInsert.setBounds(305, 159, 89, 23);
		contentPane.add(btnInsert);		btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection c = MyConnection.getConnection();
				try {
					String sql = "UPDATE `jdbc`.`personne` SET `nom` = '"+textNom.getText()+"', `prenom` = '"+textPrenom.getText()+"' WHERE (`num` = '"+textNum.getText()+"');";
					PreparedStatement pst = c.prepareStatement(sql);
					int rs = pst.executeUpdate();
					// Reset Text Fields
					textNum.setText("");
					textNom.setText("");
					textPrenom.setText("");
					JOptionPane.showMessageDialog(null, "Modifié !!!!");
					fillData();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnUpdate.setBounds(305, 125, 89, 23);
		contentPane.add(btnUpdate);		btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Connection c = MyConnection.getConnection();
				try {
					String sql = "DELETE FROM `jdbc`.`personne` WHERE (`num` = '"+textNum.getText()+"');";
					PreparedStatement pst = c.prepareStatement(sql);
					int rs = pst.executeUpdate();
					// Reset Text Fields
					textNum.setText("");
					textNom.setText("");
					textPrenom.setText("");
					JOptionPane.showMessageDialog(null, "Effacé !!!!");
					fillData();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
		btnDelete.setBounds(305, 193, 89, 23);
		contentPane.add(btnDelete);		textNum = new JTextField();
		textNum.setBackground(Color.CYAN);
		textNum.setBounds(338, 23, 86, 20);
		contentPane.add(textNum);
		textNum.setColumns(10);		JLabel lblNum = new JLabel("Num");
		lblNum.setForeground(Color.RED);
		lblNum.setBackground(Color.WHITE);
		lblNum.setBounds(282, 26, 46, 14);
		contentPane.add(lblNum);		textNom = new JTextField();
		textNom.setBackground(Color.CYAN);
		textNom.setBounds(338, 54, 86, 20);
		contentPane.add(textNom);
		textNom.setColumns(10);		textPrenom = new JTextField();
		textPrenom.setBackground(Color.CYAN);
		textPrenom.setText("");
		textPrenom.setBounds(338, 85, 86, 20);
		contentPane.add(textPrenom);
		textPrenom.setColumns(10);		JLabel lblNom = new JLabel("Nom");
		lblNom.setForeground(Color.RED);
		lblNom.setBounds(282, 57, 46, 14);
		contentPane.add(lblNom);		JLabel lblPrnom = new JLabel("Pr\u00E9nom");
		lblPrnom.setForeground(Color.RED);
		lblPrnom.setBounds(282, 88, 46, 14);
		contentPane.add(lblPrnom);
}
protected void fillData() {
//	Connection c = MyConnection.getConnection();
//	try {
//		String sql = "select * from personne";
//		PreparedStatement pst = c.prepareStatement(sql);
//		ResultSet rs = pst.executeQuery();
//		table.setModel(DbUtils.resultSetToTableModel(rs));
//	} catch ( Exception ex) {
//		ex.printStackTrace();
//	}
	System.out.println("pas de connexion à la maison !");
}
}

