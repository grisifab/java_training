package tp2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		  String nom = Paire.class.getName();                
		  try {
		    //On crée un objet Class
		    Class cl = Class.forName(nom);
		    System.out.println("1 : " + cl);
		    
		    //Nouvelle instance de la classe Paire
		    Object o = cl.newInstance();
		    System.out.println("2 : " + o);

		    //On crée les paramètres du constructeur
		    Class[] types = new Class[]{String.class, String.class};
		    for (int i = 0; i < types.length; i++){
		    	System.out.println("3." + i + " : " + types[i]);
		    }
		    //On récupère le constructeur avec les deux paramètres
		    Constructor ct = cl.getConstructor(types);
		    System.out.println("4 : " + ct);

		    //On instancie l'objet avec le constructeur surchargé !
		    Object o2 = ct.newInstance(new String[]{"valeur 1 ", "valeur 2"} );
		    System.out.println("5 : " + o2);

		  } catch (SecurityException e) {
		    e.printStackTrace();
		  } catch (IllegalArgumentException e) {
		    e.printStackTrace();
		  } catch (ClassNotFoundException e) {
		    e.printStackTrace();
		  } catch (InstantiationException e) {
		    e.printStackTrace();
		  } catch (IllegalAccessException e) {
		    e.printStackTrace();
		  } catch (NoSuchMethodException e) {
		    e.printStackTrace();
		  } catch (InvocationTargetException e) {
		    e.printStackTrace();
		  }
		
		
	}

}
