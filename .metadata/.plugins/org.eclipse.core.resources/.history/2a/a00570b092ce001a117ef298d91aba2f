package TP1.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import TP1.mdl.Mesure;
import TP1.mdl.PtsFct;

public class PtsFctDAO extends DAO<PtsFct> {
	
	private static PtsFct global;
	
	public PtsFctDAO(Connection conn) {
		super(conn);
	}

	public boolean init(){
		global = find(0);
		if (global == null){
			global = new PtsFct();
			System.out.println("id 0 pas trouvé");
		}
		return true;
	}
	
	public boolean save(){
		return true;
	}
	
	public PtsFct find(int id) {
		PtsFct ptsFct = new PtsFct();

		try {
			ResultSet result = this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeQuery(
					"SELECT * FROM ptsFct WHERE id = " + id);

			if (result.first()) {

				// tableaux gaussiennes richesse et pression essence
				int rGauss[] = new int[21];
				int pGauss[] = new int[21];
				String richesse, pression;
				for (int i = 0; i < 21; i++) {
					richesse = 'r' + Integer.toString(i);
					pression = 'p' + Integer.toString(i);
					rGauss[i] = result.getInt(richesse);
					pGauss[i] = result.getInt(pression);
				}

				ptsFct = new PtsFct(id, result.getInt("nMin"),
						result.getInt("nMax"), result.getFloat("pAirMin"),
						result.getFloat("pAirMax"), result.getInt("nbTot"),
						result.getFloat("rMin"), result.getFloat("rMax"),
						result.getFloat("rMoy"), rGauss,
						result.getFloat("pMin"), result.getFloat("pMax"),
						result.getFloat("pMoy"), pGauss);
			}
			result.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ptsFct;
	}
	
	public float[][] getAll(){
		
		int i = 0;
		int nbLignes = 0;
		float resulTot[][];

		try {
			
			ResultSet result = this.connect.createStatement(
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY).executeQuery(
					//"SELECT rMoy pMoy nbTot FROM ptsFct");
					"SELECT * FROM ptsFct");
			
			result.last();
			nbLignes = result.getRow();
			
			result.beforeFirst();
			
			resulTot = new float [nbLignes][3];
			System.out.println(resulTot.length);
			
			for(i = 0; i < nbLignes; i++){
				result.next();
				resulTot[i][0] = result.getFloat("rMoy");
				resulTot[i][1] = result.getFloat("pMoy");
				resulTot[i][2] = result.getInt("nbTot");
			}

			
			result.close();
			return resulTot;

		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public boolean addPoint (Mesure mesure){
		// rajoute la mesure au point de fonctionnement correspondant
		
		PtsFct local;
		int nbTot = global.getNbTot(), nbLoc;
		int indGaussR, indGaussP;	// indices du tableau
		float rMesGauss, pMesGauss;	// limités aux bornes de la gaussienne
		int tab[];
		float moy;
		int indBDD;
		
		// bornage valeurs
		float nMes =(float) Math.min(mesure.getRegime(), 10000);
		nMes = Math.max(nMes, 0);
		
		float pAirMes =(float) Math.min(mesure.getPressAir(), 0.99);
		pAirMes =(float) Math.max(pAirMes, 0);
		
		float pMes =(float) Math.min(mesure.getPressEss(), 8);
		pMes =(float) Math.max(pMes, 0);
		
		float rMes =(float) Math.min(mesure.getRichesse(), 2);
		rMes = Math.max(rMes, 0);
		
		
		// verif si point de fonctionnement déjà présent dans la BDD
		// si le régime n'existe pas on le crée
		while((global.getnMax()-100)< nMes){
			createNZone(global.getnMax());
			global.setnMax((global.getnMax()+100));
		}

		// on va chercher le point existant dans la BDD
		indBDD = (int) ((Math.floor(nMes / 100) * 20) + Math.floor(pAirMes / 0.05) + 1);
		//System.out.println("indBDD = " + indBDD);
		local = find(indBDD);
		nbLoc = local.getNbTot();

		// calcul pour gaussienne
		rMesGauss =(float) Math.min(rMes, 1.5);
		rMesGauss =(float) Math.max(rMesGauss, 0.5);
		indGaussR = (int) Math.floor((rMesGauss-0.475)/0.05);
		
		pMesGauss =(float) Math.min(pMes, 4);
		pMesGauss =(float) Math.max(pMesGauss, 0);
		indGaussP = (int) Math.floor((pMesGauss+0.1)/0.2);
		
		// mise à jour du ptsFct global et local
		
		// Bornes Min/max de la zone globale uniquement
		global.setnMin(Math.min(global.getnMin(),(int)nMes));
		global.setpAirMin(Math.min(global.getpAirMin(),pAirMes));
		global.setpAirMax(Math.max(global.getpAirMax(),pAirMes));
		
		// richesse min, max, moy en global et local
		global.setrMin(Math.min(global.getrMin(),rMes));
		global.setrMax(Math.max(global.getrMax(),rMes));
		moy = ((global.getrMoy()*nbTot)+ rMes)/(float)(nbTot+1);
		global.setrMoy(moy);
		local.setrMin(Math.min(local.getrMin(),rMes));
		local.setrMax(Math.max(local.getrMax(),rMes));
		moy = ((local.getrMoy()*nbLoc)+ rMes)/(float)(nbLoc+1);
		local.setrMoy(moy);
		
		// pression essence min, max, moy en global et local
		global.setpMin(Math.min(global.getpMin(),pMes));
		global.setpMax(Math.max(global.getpMax(),pMes));
		moy = ((global.getpMoy()*nbTot)+ pMes)/(float)(nbTot+1);
		global.setpMoy(moy);
		local.setpMin(Math.min(local.getpMin(),pMes));
		local.setpMax(Math.max(local.getpMax(),pMes));
		moy = ((local.getpMoy()*nbLoc)+ pMes)/(float)(nbLoc+1);
		local.setpMoy(moy);
		
		// gaussienne richesse en global et local
		tab = global.getrGauss();
		tab[indGaussR]++;
		global.setrGauss(tab);
		tab = local.getrGauss();
		tab[indGaussR]++;
		local.setrGauss(tab);
		
		// gaussienne pression essence en global et local
		tab = global.getpGauss();
		tab[indGaussP]++;
		global.setpGauss(tab);
		tab = local.getpGauss();
		tab[indGaussP]++;
		local.setpGauss(tab);
		
		// nombre occurrences
		global.setNbTot((nbTot+1));
		local.setNbTot((nbLoc+1));
		
		// mise à jour BDD du local uniquement, le global sera fait à la fin du traitement
		update(local,indGaussR,indGaussP);
				
		return true;
	}
	
	
	public boolean createNZone(int nMin) {
		// rajoute dans la BDD tous les points de fonctionnement pour ce régime
		// définition du rang en fct du régime (20 cases par rang)
		int idSsPress = 20 * (nMin / 100);

		try {
			PreparedStatement prepa = this.connect
					.prepareStatement("INSERT INTO ptsFct (id, nMin, nMax, pAirMin,pAirMax) VALUES (?,?,?,?,?);");

			for (int i = 1; i <= 20; i++) {
				prepa.setInt(1, (idSsPress + i));
				prepa.setInt(2, nMin);
				prepa.setInt(3, (nMin + 100));
				prepa.setFloat(4, (float) ((i - 1) * 0.05));
				prepa.setFloat(5, (float) (i * 0.05));
				prepa.execute();
			}
			prepa.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}


	@Override
	public boolean update(PtsFct obj, int indR, int indP) {
		
		try {
			PreparedStatement prepa = this.connect
					.prepareStatement("UPDATE ptsFct " +
							"SET nbTot = ? ," +
							"rMin = ? ," +
							"rMax = ? ," +
							"rMoy = ? ," +
							"pMin = ? ," +
							"pMax = ? ," +
							"pMoy = ? ," +
							"r? = ? ," +
							"p? = ? " +
							"WHERE id = ? ;");

				prepa.setInt(1, obj.getNbTot());
				prepa.setFloat(2, obj.getrMin());
				prepa.setFloat(3, obj.getrMax());
				prepa.setFloat(4, obj.getrMoy());
				prepa.setFloat(5, obj.getpMin());
				prepa.setFloat(6, obj.getpMax());
				prepa.setFloat(7, obj.getpMoy());
				prepa.setInt(8, indR);
				prepa.setInt(9, obj.getrGauss()[indR]);
				prepa.setInt(10,indP);
				prepa.setInt(11, obj.getpGauss()[indP]);
				prepa.setInt(12, obj.getId());
				prepa.execute();
				prepa.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return false;
	}
	
	@Override
	public boolean create(PtsFct obj) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(PtsFct obj) {
		// TODO Auto-generated method stub
		return false;
	}
}