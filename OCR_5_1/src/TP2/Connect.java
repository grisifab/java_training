package TP2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

public class Connect {
	public static void main(String[] args) {      
	  try {
	    Class.forName("com.mysql.jdbc.Driver");
	    System.out.println("Driver O.K.");
	
	    String url = "jdbc:mysql://localhost:3306/Ecole";
	    String user = "root";
	    String passwd = "root";
	
	    Connection conn = DriverManager.getConnection(url, user, passwd);
	    System.out.println("Connexion effective !");    
	    
	  //Création d'un objet Statement
	      Statement state = conn.createStatement();
	      //L'objet ResultSet contient le résultat de la requête SQL
	      ResultSet result = state.executeQuery("SELECT * FROM professeur");
	      //On récupère les MetaData
	      ResultSetMetaData resultMeta = result.getMetaData();
	         
	      System.out.println("\n**********************************");
	      System.out.println("Il y a " + resultMeta.getColumnCount() + " colonnes");
	      //On affiche le nom des colonnes
	      for(int i = 1; i <= resultMeta.getColumnCount(); i++)
	        System.out.print(resultMeta.getColumnName(i).toUpperCase() + "\n");
	         
	      System.out.println("\n**********************************");
	         
	      while(result.next()){         
	          System.out.print("\t" + result.getString("prof_nom") + "\t" + " | " + "\t" + result.getString("prof_prenom") + "\n");
	      }

	      result.close();
	      state.close();
	       
	  } catch (Exception e) {
	    e.printStackTrace();
	  }      
	}
}