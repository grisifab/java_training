package TP5;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class Fenetre extends JFrame {

	private JTable tableau;
	private JButton change = new JButton("Changer la taille");

	public Fenetre() {
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("JTable");
		this.setSize(600, 180);

		Object[][] data = { { "Cysboy", "6boy", "Combo", new Boolean(true) },
				{ "BZHHydde", "BZH", "Combo", new Boolean(false) },
				{ "IamBow", "BoW", "Combo", new Boolean(false) },
				{ "FunMan", "Year", "Combo", new Boolean(true) } };

		String title[] = { "Pseudo", "Age", "Taille", "OK ?" };

		this.tableau = new JTable(data, title);
		this.tableau.setRowHeight(30);
		this.tableau.getColumn("Age").setCellRenderer(new ButtonRenderer());
		this.tableau.getColumn("Taille").setCellRenderer(new ComboRenderer());
		this.getContentPane()
				.add(new JScrollPane(tableau), BorderLayout.CENTER);

	}

	public class ButtonRenderer extends JButton implements TableCellRenderer {

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean isFocus, int row,
				int col) {
			// On écrit dans le bouton ce que contient la cellule
			setText((value != null) ? value.toString() : "");
			// On retourne notre bouton
			return this;
		}
	}

	public class ComboRenderer extends JComboBox implements TableCellRenderer {

		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean isFocus, int row,
				int col) {
			this.addItem("Très bien");
			this.addItem("Bien");
			this.addItem("Mal");
			return this;
		}
	}

	public static void main(String[] args) {
		Fenetre fen = new Fenetre();
		fen.setVisible(true);
	}
}