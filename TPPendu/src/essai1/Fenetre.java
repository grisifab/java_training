package essai1;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class Fenetre extends JFrame {
	private JTabbedPane onglets = new JTabbedPane ();
	JPanel contAcceuil = new JPanel();
	public ArrayList<Score> players = new ArrayList<Score>();
	public Score player;
	private JComboBox choixP;
	private Fichiers fichiers = new Fichiers();

	public Fenetre() {
		this.setTitle("Jeux du Pendu");
		this.setSize(800, 330);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		player = new Score("new");
		
		//-------------------Jeux-------------------------------
		Dimension size = new Dimension(this.getWidth(), this.getHeight());
		Jeux jeux = new Jeux(size, player);
		
		// ------------------Regles-----------------------------
		size = new Dimension(537, 330);
		Pagimage regles = new Pagimage(size, "images/regles.jpg");
		
		// ------------------Acceuil----------------------------
		
		acceuil();
		
		//-------------------HiScoreq---------------------------
		
		size = new Dimension(this.getWidth(), this.getHeight());
		HiScore hiScore = new HiScore(size, player);
		
		//-------------------Onglets----------------------------
		onglets.add("Acceuil",contAcceuil);
		onglets.add("Regles",regles.getPanel());
		onglets.add("Jeux",jeux.getPanel());
		onglets.add("Hi-Scores", hiScore.getPanel());
		
		//this.setContentPane(contJeux);
		this.getContentPane().add(onglets);
		this.setVisible(true);
	}
	
	private void initJeux(){
		// mise à jour du joueur
		Dimension size = new Dimension(this.getWidth(), this.getHeight());
		Jeux jeux = new Jeux(size, player);
		onglets.remove(2);
		onglets.add("Jeux",jeux.getPanel());
	}
	
	private void acceuil(){
		
		Box partieG = Box.createVerticalBox();
		
		// titre
		JLabel jlbl = new JLabel();
		jlbl.setText("Bienvenu dans le jeux du Pendu !");
		jlbl.setFont(new Font("Comics Sans MS", Font.BOLD, 20));
		jlbl.setAlignmentX((float)0.5);
		partieG.add(jlbl);
		
		partieG.add(Box.createRigidArea(new Dimension(45, 45)));
		
		// choix joueur
		choixP = new JComboBox();
		choixP.addItem("Nouveau Joueur");
		choixP.setSelectedIndex(0);
		players = fichiers.getPlayers();
		System.out.println(players.size());
		for (int i = 0; i < players.size(); i++){
			choixP.addItem(players.get(i).getNom()+ ", " + players.get(i).getNbPts() + " points, " + players.get(i).getNbMots() + " mots trouvés.");
		}
		choixP.addActionListener(new comboAction());
		choixP.setAlignmentX((float)0.5);
		partieG.add(choixP);
		
		// image
		Dimension size = new Dimension(330, 264);
		Pagimage acceuil = new Pagimage(size, "images/acceuil.jpg");
		
		contAcceuil.setBackground(Color.white);
		contAcceuil.add(partieG);
		contAcceuil.add(acceuil.getPanel());
	}
	
	class comboAction implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			int indice = choixP.getSelectedIndex();
			if ( indice == 0){
				// nouveau joueur
				JOptionPane jop = new JOptionPane();
				String nom = jop.showInputDialog(null, "Votre nom ?", "Nouveau Joueur", JOptionPane.QUESTION_MESSAGE);
				player.setNom(nom);
				player.setId(players.size()+1);
				player.setNbMots(0);
				player.setNbPts(0);
			} else
			{
				player = players.get(indice-1);
			}
			System.out.println(player);
			initJeux();
		}	
	}
	
	

}