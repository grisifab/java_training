package essai1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

public class Pagimage extends ZContainerImg {
	
	private ImageLabel imageLabel;
	private String chemin;
	private Dimension dimension;

	public Pagimage(Dimension dim, String str) {
		super(dim, str);
		chemin = str;
		dimension = dim;
		initPanel();
	}

	protected void initPanel() {
		this.imageLabel = new ImageLabel();
		this.imageLabel.setPreferredSize(dimension);
		this.imageLabel.setImagePath(chemin);
		this.panel.add(imageLabel);
	}

	
}
