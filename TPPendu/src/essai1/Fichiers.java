package essai1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import com.google.gson.Gson;

public class Fichiers {
	
	public String dico(){
		int nbre, i = 0, j = 0;
		String mot = "DOMMAGE", accents = "ÀÂÄÇÉÊÈËÎÏÔÖÙÛÜ", ssAccent = "AAACEEEEIIOOUUU";
		try
		{
			File file = new File("dico/dictionnaire.txt");
			//FileReader fr = new FileReader(file);			// remplacé par le stream car ne fonctionnais pas avec les accents
			//BufferedReader br = new BufferedReader(fr);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"ISO-8859-1"));
			nbre = (int)(Math.random()*336529);
			while(i<nbre){
				mot = br.readLine();
				i++;
			}
			//fr.close();
		}
		catch (IOException e){
			e.printStackTrace();
		} 
		mot = mot.toUpperCase();
		
		for(i = 0; i < accents.length(); i++){	// boucle sur lettre avec accent
			mot = mot.replace(accents.charAt(i), ssAccent.charAt(i));	
		}
		System.out.println(mot);
		return mot;
	}
	
	public ArrayList<Score> getPlayers () {
		Gson gson = new Gson();
		ArrayList<Score> players = new ArrayList<Score>();
		
		try{
			File file = new File("score/score.json");
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String ligne = br.readLine();
			while(ligne != null){
				players.add(gson.fromJson(ligne, Score.class));
				ligne = br.readLine();
			}
			fr.close();
		}catch (IOException e){
			e.printStackTrace();
		}
		return players;
	}
	
	public void savePlayer (Score player) {
		Gson gson = new Gson();
		String jsonString;
		ArrayList<Score> players = getPlayers ();
		int Id = player.getId();
		int taille = players.size();
		System.out.println("Id : " + Id);
		System.out.println("taille players " + taille);
		// mise à jour du joueur dans la liste
		if(Id > taille)	// nouveau joueur
		{
			System.out.println("rajout");
			players.add(player);
			taille++;
		} else
		{
			System.out.println("modif");
			Id--;
			players.remove(Id);
			players.add(Id, player);
		}
		System.out.println(players);
		//ecriture liste dans fichier
		try{
			File file = new File("score/score.json");
			FileWriter fw = new FileWriter(file);
			//BufferedWriter bw = new BufferedWriter(fw);
			for(int i = 0; i < taille; i++)
			{
				if(i != 0) {fw.write("\n");;}
				jsonString = gson.toJson(players.get(i));
				fw.write(jsonString);
				System.out.println("ici :" + i);
			}
			fw.close();
		}catch (IOException e){
			e.printStackTrace();
		}
	}
}
