package essai1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

public class HiScore extends ZContainer {

	// liste joueurs
	JLabel jlbl = new JLabel();
	private ArrayList<Score> players;
	private JTable tableau;
	
	// Jeux
	private ImageLabel imageLabel;
	private Box partieG;
	private ArrayList<Box> bx = new ArrayList<Box>();// pour le clavier
	private ArrayList<JButton> boutons = new ArrayList<JButton>();// touches du clavier
	private Score playerActu;
	private JLabel lblNbTrv = new JLabel();
	private JLabel lblScr = new JLabel();
	private JLabel lblMot = new JLabel();
	private Fichiers fichiers = new Fichiers();
	private int nbErr = 0;
	private String leMot, leMotAff;
	String lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private ArrayList<Boolean> choixLettres = new ArrayList<Boolean>();

	public HiScore(Dimension dim,Score player) {
		super(dim, player);
		playerActu = player;
		initPanel();
	}

	protected void initPanel() {
		
		players = fichiers.getPlayers();
		createTable();
		
		jlbl.setText("TOP 10 !");
		jlbl.setFont(new Font("Comics Sans MS", Font.BOLD, 20));
		jlbl.setAlignmentX((float)0.5);
		this.panel.setLayout(new BorderLayout());
		this.panel.add(jlbl,BorderLayout.NORTH);
		this.panel.add(tableau,BorderLayout.SOUTH);
	}

	private void createTable(){
	      //Données de notre tableau
	      Object[][] data = {  
	         {"Nom", "Mots", "Points"}
	      };
	      Collections.sort(players, new CustomComparator());
	 
	      String  title[] = {"Nom", "Mots", "Points"};    
	      ZModel zModel = new ZModel(data, title); 
	      this.tableau = new JTable(zModel);     
	      this.tableau.setRowHeight(20);
	      for (int i = 0; i < Math.min(players.size(),10);i++)
	      {
	    	  Object[] donnee = new Object[]
			            {players.get(i).getNom(), players.get(i).getNbMots(), players.get(i).getNbPts()};

		      ((ZModel)this.tableau.getModel()).addRow(donnee);
	      }
	      
	   }
	
	public class CustomComparator implements Comparator<Score> {
	    public int compare(Score o1, Score o2) {
	        return (o1.getNbPts() < o2.getNbPts()? 1 : -1);
	    }
	}

}
