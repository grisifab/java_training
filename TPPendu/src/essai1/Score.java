package essai1;

public class Score {
	
	private static int cnt;
	private int Id;
	private String nom;
	private int nbMots;
	private int nbPts;
	
	
	@Override
	public String toString() {
		return Id + " Score [nom=" + nom + ", nbMots=" + nbMots + ", nbPts=" + nbPts
				+ "]";
	}

	public Score(String nom, int nbMots, int nbPts) {
		super();
		this.Id = cnt++;
		this.nom = nom;
		this.nbMots = nbMots;
		this.nbPts = nbPts;
	}

	public Score(String nom) {
		super();
		this.Id = cnt++;
		this.nom = nom;
		this.nbMots = 0;
		this.nbPts = 0;
	}
	
	public void AddScrore(int scorePartie){
		this.nbMots ++;
		this.nbPts += scorePartie;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public int getNbMots() {
		return nbMots;
	}
	public void setNbMots(int nbMots) {
		this.nbMots = nbMots;
	}
	public int getNbPts() {
		return nbPts;
	}
	public void setNbPts(int nbPts) {
		this.nbPts = nbPts;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

}
