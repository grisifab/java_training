package essai1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class Jeux extends ZContainer implements ActionListener {

	// Jeux
	private ImageLabel imageLabel;
	private Box partieG;
	private ArrayList<Box> bx = new ArrayList<Box>();// pour le clavier
	private ArrayList<JButton> boutons = new ArrayList<JButton>();// touches du clavier
	private Score playerActu;
	private JLabel lblNbTrv = new JLabel();
	private JLabel lblScr = new JLabel();
	private JLabel lblMot = new JLabel();
	private Fichiers fichiers = new Fichiers();
	private int nbErr = 0;
	private String leMot, leMotAff;
	String lettres = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private ArrayList<Boolean> choixLettres = new ArrayList<Boolean>();

	public Jeux(Dimension dim,Score player) {
		super(dim, player);
		playerActu = player;
		initPanel();
	}

	protected void initPanel() {
		// ----------------Jeux-------------------
		// créations des boutons et du vecteur de 26 booléens
		creaBoutons();
		// recuperation mot
		leMot = fichiers.dico();
		leMotAff = cache(leMot);
		// affichage et clavier
		partieGauche();
		// image
		partieDroite();
		this.panel.setBackground(Color.white);
		this.panel.setSize(800, 330);
		this.panel.add(partieG);
		this.panel.add(Box.createRigidArea(new Dimension(30, 30)));
		this.panel.add(imageLabel);
	}

	public void actionPerformed(ActionEvent arg0) {
		int ind;
		char lettre;
		String chemin;
		JButton boutonAppuye = (JButton) arg0.getSource();
		lettre = (boutonAppuye.getText()).charAt(0);
		ind = lettres.indexOf(lettre); // Nème lettre de l'alphabet
		choixLettres.set(ind, true);
		boutons.get(ind).setEnabled(false);

		if (leMot.contains(Character.toString(lettre))) { // lettre présente
															// dans le mot
			leMotAff = cache(leMot);
			lblMot.setText(leMotAff);
			if (!leMotAff.contains("*"))// terminé
			{
				gagne();
			}
		} else { // lettre non présente dans le mot
			nbErr++;
			chemin = "images/img" + nbErr + ".jpg";
			this.imageLabel.setImagePath(chemin);
			this.imageLabel.repaint();
			if (nbErr > 6) {
				perdu();
			}
		}
	}
	
	void gagne() {

		JOptionPane popup = new JOptionPane();
		int gains[] = { 100, 50, 35, 25, 15, 10, 5 };
		int gain = gains[nbErr];

		// popup avec les points gagnés
		popup.showMessageDialog(null, "Bravo, vous avez gagné, avec " + nbErr
				+ " erreurs. \nVous augmentez votre score de " + gain
				+ " points !", "Twingo !", JOptionPane.INFORMATION_MESSAGE);

		// incrément mots trouvé et score
		playerActu.AddScrore(gain);
		lblNbTrv.setText("Nombre de mots trouvés : " + playerActu.getNbMots());
		lblScr.setText("Score actuel : " + playerActu.getNbPts());
		fichiers.savePlayer (playerActu);
		
		raz();
	}

	void perdu() {
		JOptionPane popup = new JOptionPane();

		// popup avec les points gagnés
		popup.showMessageDialog(null,
				"Désolé, vous avez perdu. \nLe mot était " + leMot + " !",
				"Bléreau !", JOptionPane.INFORMATION_MESSAGE);	
		raz();
	}

	void raz() {
		// RAZ
		nbErr = 0;
		for (int i = 0; i < 26; i++) {
			choixLettres.set(i, false);
			boutons.get(i).setEnabled(true);
		}

		// nouveau mot
		leMot = fichiers.dico();
		leMotAff = cache(leMot);
		lblMot.setText(leMotAff);

		// nouveau dessin
		this.imageLabel.setImagePath("images/img0.jpg");
		this.imageLabel.repaint();
	}

	void partieGauche() {
		partieG = Box.createVerticalBox();

		// nb de mots trouvés
		lblNbTrv.setText("Nombre de mots trouvés : " + playerActu.getNbMots());
		lblNbTrv.setAlignmentX((float)0.5);
		partieG.add(lblNbTrv);
		partieG.add(Box.createRigidArea(new Dimension(15, 15)));

		// score actuel
		lblScr.setText("Score actuel : " + playerActu.getNbPts());
		lblScr.setAlignmentX((float)0.5);
		partieG.add(lblScr);
		partieG.add(Box.createRigidArea(new Dimension(15, 15)));

		// mot à trouver
		lblMot.setText(leMotAff);
		lblMot.setAlignmentX((float)0.5);
		Font police = new Font("Tahoma", Font.BOLD, 24);
		lblMot.setFont(police);
		lblMot.setForeground(Color.blue);
		partieG.add(lblMot);
		partieG.add(Box.createRigidArea(new Dimension(15, 15)));

		// clavier
		bx.get(4).setAlignmentX((float)0.5);
		partieG.add(bx.get(4));
	}

	void creaBoutons() {
		Dimension buttonDimension = new Dimension(50,30);
		int i, j;
		// les lettres
		for (i = 0; i < 26; i++) {
			boutons.add(new JButton(Character.toString(lettres.charAt(i))));
			choixLettres.add(false);
			boutons.get(i).addActionListener(this);
			boutons.get(i).setPreferredSize(buttonDimension);
		}
		i = 0;
		// les boxs horizontales
		for (j = 0; j < 4; j++) {
			bx.add(Box.createHorizontalBox());
			for (i = 0; i < 7; i++) {
				if ((j * 7 + i) < 26) // 26 lettres mais 4 * 7 = 28
					bx.get(j).add(boutons.get(j * 7 + i));
			}
		}
		// la mega box
		bx.add(Box.createVerticalBox());
		for (j = 0; j < 4; j++) {
			bx.get(4).add(bx.get(j));
		}
	}

	void partieDroite() {
		this.imageLabel = new ImageLabel();
		this.imageLabel.setPreferredSize(new Dimension(350, 320));
		this.imageLabel.setImagePath("images/img0.jpg");
	}

	String cache(String mot) {
		String cache = "";
		int ind;
		for (int i = 0; i < mot.length(); i++) {

			ind = lettres.indexOf(mot.charAt(i)); // Nème lettre de l'alphabet
			if (choixLettres.get(ind)) {
				cache += mot.charAt(i); // lettre choisie
			} else {
				cache += "*"; // lettre non choisie
			}
		}
		return cache;
	}

}
