package TP1;

import java.awt.BorderLayout;
import java.awt.Color; 
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Fenetre extends JFrame implements ActionListener {
  private Panneau pan = new Panneau();
  private Bouton bouton = new Bouton("mon bouton");
  private Bouton bouton2 = new Bouton("mo bouto");
  private JPanel container = new JPanel();
  JLabel label1 = new JLabel();
  private int compteur = 0;
  
  
  public Fenetre (){
    this.setTitle("Animation");
    this.setSize(300, 300);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.setLocationRelativeTo(null);
    
    
    label1.setText("Mon premier JLabel");
    //Définition d'une police d'écriture
    Font police = new Font("Tahoma", Font.BOLD, 16);
    //On l'applique au JLabel
    label1.setFont(police);
    //Changement de la couleur du texte
    label1.setForeground(Color.blue);
    //On modifie l'alignement du texte grâce aux attributs statiques
    //de la classe JLabel
    label1.setHorizontalAlignment(JLabel.CENTER);
    
    container.setBackground(Color.white);
    container.setLayout(new BorderLayout());
    container.add(pan, BorderLayout.CENTER);
    container.add(bouton, BorderLayout.SOUTH);
    container.add(bouton2, BorderLayout.EAST);
    container.add(label1, BorderLayout.NORTH);
    
    bouton.addActionListener(this);
    
    this.setContentPane(container);
    this.setVisible(true);    
    go();
  }
  
  private void go(){  
    //Les coordonnées de départ de notre rond
    int x = pan.getPosX(), y = pan.getPosY();
    //Le booléen pour savoir si l'on recule ou non sur l'axe x
    boolean backX = false;
    //Le booléen pour savoir si l'on recule ou non sur l'axe y
    boolean backY = false;
    
    //Dans cet exemple, j'utilise une boucle while
    //Vous verrez qu'elle fonctionne très bien
    while(true){
      //Si la coordonnée x est inférieure à 1, on avance
      if(x < 1)backX = false;
      //Si la coordonnée x est supérieure à la taille du Panneau moins la taille du rond, on recule
      if(x > pan.getWidth()-50)backX = true;
      //Idem pour l'axe y
      if(y < 1)backY = false;
      if(y > pan.getHeight()-50)backY = true;
      
      //Si on avance, on incrémente la coordonnée
      if(!backX)
        pan.setPosX(++x);
      //Sinon, on décrémente
      else
        pan.setPosX(--x);
      //Idem pour l'axe Y
      if(!backY)
        pan.setPosY(++y);
      else
        pan.setPosY(--y);
        
      //On redessine notre Panneau
      pan.repaint();
      //Comme on dit : la pause s'impose ! Ici, trois millièmes de seconde
      try {
        Thread.sleep(3);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }    
  }

public void actionPerformed(ActionEvent arg0) {
	// TODO Auto-generated method stub
	this.compteur++;
    label1.setText("Vous avez cliqué " + this.compteur + " fois");
}  
}