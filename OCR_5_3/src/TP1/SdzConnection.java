package TP1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;



public class SdzConnection{

private static String url = "jdbc:mysql://localhost:3306/Ecole";
private static String user = "root";
private static String passwd = "root";
private static Connection connect;
 
public static Connection getInstance(){
  if(connect == null){
	  
	  
	  try {
    	connect = DriverManager.getConnection(url, user, passwd);
    } catch (SQLException e) {
      e.printStackTrace();
    } 
  }      
  return connect;
}   
}
