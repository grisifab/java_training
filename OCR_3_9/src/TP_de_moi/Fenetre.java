package TP_de_moi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

public class Fenetre extends JFrame {
	private Panneau pan = new Panneau();
	private JPanel container = new JPanel();
	
	private JMenuBar menuBar = new JMenuBar();
	private JMenu 
			fichier = new JMenu("Fichier"),
			dessin = new JMenu("Dessin"),
			couleur = new JMenu("Couleur"),
			pinceau = new JMenu("Pinceau");
	private JMenuItem
			quitter = new JMenuItem("Quitter"),
			effacer = new JMenuItem("Effacer");
	private JRadioButtonMenuItem
			rond = new JRadioButtonMenuItem("Rond"),
			carre = new JRadioButtonMenuItem("Carré"),
			bleu = new JRadioButtonMenuItem("Bleu"),
			rouge = new JRadioButtonMenuItem("Rouge"),
			vert = new JRadioButtonMenuItem("Vert");
	
	private ButtonGroup bgc = new ButtonGroup();
	private ButtonGroup bgf = new ButtonGroup();

	// Création de notre barre d'outils
	private JToolBar toolBar = new JToolBar();

	// Les boutons de la barre d'outils
	private JButton bCarre = new JButton(new ImageIcon("pCarre.jpg")),
			bRond = new JButton(new ImageIcon("rond.jpg"));

	private CouleurListener chgCouleur = new CouleurListener();
	private FormeListener chgForme = new FormeListener();

	private int taille = 10;
	private Color color = Color.blue;
	private String type = "ROND";
	private ArrayList<Point> points = new ArrayList<Point>();  

	public Fenetre() {
		this.setTitle("PaintSbrush");
		this.setSize(500, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		container.setBackground(Color.white);
		container.setLayout(new BorderLayout());		
		container.add(pan, BorderLayout.CENTER);
		
		// rajout des listeners
		quitter.addActionListener(new QuitterListener());
		effacer.addActionListener(new EffacerListener());
		rond.addActionListener(chgForme);
		carre.addActionListener(chgForme);
		bleu.addActionListener(chgCouleur);
		rouge.addActionListener(chgCouleur);
		vert.addActionListener(chgCouleur);
		
		// rajout des raccourcis
		quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
		effacer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK));
		rond.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_MASK));
		carre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_MASK));
		bleu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, KeyEvent.SHIFT_MASK));
		rouge.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.SHIFT_MASK));
		vert.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, KeyEvent.SHIFT_MASK));
		
		//couleurs
		bgc.add(bleu);
		bgc.add(rouge);
		bgc.add(vert);
		bleu.setSelected(true);
		
		//formes
		bgf.add(rond);
		bgf.add(carre);
		rond.setSelected(true);
		
		
		fichier.add(effacer);
		fichier.add(quitter);
		fichier.setMnemonic('F');
		menuBar.add(fichier);
		
		couleur.add(bleu);
		couleur.add(rouge);
		couleur.add(vert);
		couleur.setMnemonic('C');
		dessin.add(couleur);
		pinceau.add(rond);
		pinceau.add(carre);
		pinceau.setMnemonic('P');
		dessin.add(pinceau);
		dessin.setMnemonic('D');
		menuBar.add(dessin);

		pan.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				points.add(new Point(e.getX() - (taille / 2), e.getY()- (taille / 2), taille, color,type));
				pan.modif(points);
				//System.out.println(points);
			}
		});
		
		pan.addMouseMotionListener(new MouseMotionListener() {
			public void mouseDragged(MouseEvent e) {
				points.add(new Point(e.getX() - (taille / 2), e.getY()- (taille / 2), taille, color,type));
				pan.modif(points);
				//System.out.println(points);
			}

			public void mouseMoved(MouseEvent e) {
			}
		});
		
		this.setJMenuBar(menuBar);
		this.setContentPane(container);
		this.initToolBar();
		this.setVisible(true);
	}
	
	  private void initToolBar(){
		    this.bRond.setEnabled(false);
		    this.bCarre.setEnabled(true);
		    this.bCarre.addActionListener(chgForme);
		    this.bRond.addActionListener(chgForme);
		    this.toolBar.add(bCarre);
		    this.toolBar.add(bRond);
		    this.toolBar.addSeparator();
		    this.add(toolBar, BorderLayout.NORTH);    
		  }
	
	
	// Listeners
	
	public class CouleurListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == vert)
				color = Color.green;
			else if (e.getSource() == bleu)
				color = Color.blue;
			else
				color = Color.red;
			System.out.println(color);
		}
	}
	
	public class FormeListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if ((e.getSource() == rond) || (e.getSource() == bRond))
			{	
				type = "ROND";
				bCarre.setEnabled(true);
				bRond.setEnabled(false);
			}
			else
			{
				type = "CARRE";
				bRond.setEnabled(true);
				bCarre.setEnabled(false);
			}
			System.out.println(type);
		}
	}
	
	public class QuitterListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			System.exit(0);
		}
	}
	
	public class EffacerListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			points = new ArrayList<Point>();
			pan.modif(points);
			System.out.println("A pu !");
		}
	}
	
	
	
}
