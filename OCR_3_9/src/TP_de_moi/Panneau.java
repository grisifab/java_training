package TP_de_moi;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JPanel;

public class Panneau extends JPanel {
	public ArrayList<Point> mespoints = new ArrayList<Point>(); 
	private Color couleurFond = Color.white;

	public void paintComponent(Graphics g) {
		g.setColor(couleurFond);
		g.fillRect(0, 0, this.getWidth(), this.getHeight());
		for(Point p : this.mespoints)
	      {
	        //On récupère la couleur
	        g.setColor(p.getColor());

	        //Selon le type de point
	        if(p.getType().equals("CARRE")){
	          g.fillRect(p.getX(), p.getY(), p.getSize(), p.getSize());
	        }
	        else{
	          g.fillOval(p.getX(), p.getY(), p.getSize(), p.getSize());
	        }
	      }
	}
	
	public void modif(ArrayList<Point> points)
	{
		mespoints = points;
		repaint();
	}
	
}