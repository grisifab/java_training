package TP1.dao;

import java.sql.Connection;
import TP1.connection.DBConnection;
import TP1.mdl.Mesure;
import TP1.mdl.PtsFct;

public abstract class DAO<T> {
  protected Connection connect = null;
   
  public DAO(Connection conn){
    this.connect = conn;
  }
   
  /**
  * Méthode de création
  * @param obj
  * @return boolean 
  */
  public abstract boolean create(T obj);

  /**
  * Méthode pour effacer
  * @param obj
  * @return boolean 
  */
  public abstract boolean delete(T obj);

  /**
  * Méthode de mise à jour
  * @param obj
  * @return boolean
  */
  public abstract boolean update(T obj, int indR, int indP);

  /**
  * Méthode de recherche des informations
  * @param id
  * @return T)
  */
  public abstract T find(int id);
  public abstract T findX2Y(int idMin, int idMax);
  public abstract float[][] getAll();
  public abstract int[][] getGauss();
  public abstract int[][] getGaussFrom(PtsFct ptsFct);
  public abstract boolean init();
  public abstract boolean createNZone (int nMin);
  public abstract boolean addPoint (Mesure mesure);
  public abstract boolean save();
  public abstract boolean erase();
  
  
}