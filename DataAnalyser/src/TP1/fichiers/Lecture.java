package TP1.fichiers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JProgressBar;

import TP1.affichage.Progress;
import TP1.dao.DAO;
import TP1.mdl.Mesure;
import TP1.mdl.PtsFct;

public class Lecture {
	
	
	
	public boolean traiteFichier (File file, DAO<PtsFct> ptsFctDAO) {
		
		JProgressBar bar  = new JProgressBar();
		
		Path path;
		long lineCount;
		int num = 0, percent = 0;
		Mesure mesure = new Mesure();
		int i =0;               				//indice dans chaine miniTampon
		int j =0;               				//indice valeurRaw
		float valeurRaw[] = new float[6];     	// j
		String miniTampon;    					// i
		String tampon = "";       				// k
		
		Progress p = new Progress(bar);			// pour affichage progression
		
		try
		{
			// récupération du global
			ptsFctDAO.init();
			
			path = Paths.get(file.getPath());
			lineCount = Files.lines(path).count();
			System.out.println(lineCount);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file),"ISO-8859-1"));
			
			for (i = 0; i <= 2; i++){
				// saut de 2 lignes puis lecture données
				tampon = br.readLine();
			}
			
			
			while(tampon != null){// traitement des données tant qu'il y en a
				// recuperation données brutes
				i = 0;
				j = 0;
				for (int k = 0; k < tampon.length(); k++) { 				// traitement caractère par caractère
					if (tampon.charAt(k) == '\t' || tampon.charAt(k) == '\n') {
						// passage d'une valeur à une autre
						miniTampon = tampon.substring(i, k);
						valeurRaw[j] = Float.parseFloat(miniTampon);
						j++; 											// passage à la minichaine suivante
						i = k + 1; 										// mise au début de la la minichaine suivante	
					}
				}
				
				// traitement valeur
				// regime
				if (valeurRaw[0] > 998){ // acqui à la seconde si pas de régime
		            mesure.setRegime(0);
		        } else {
		        	mesure.setRegime((float) (30000.0 / valeurRaw[0])); // régime pour 4cyl 4T mono-bobine
		        }
				
				
		        mesure.setPressEss((float) ((valeurRaw[1]-0.12)/0.9));   	// delta pression essence/air en bar
		        mesure.setPressAir((float) (1- ((valeurRaw[2]-0.12)/4.5)));  // pression air collecteur en bar
		        mesure.setRichesse(InterpolLin(valeurRaw[4]));       		// lambda
				
				//System.out.println(mesure);
		        num++;
		        if (num >=  (lineCount * percent / 100)){
		        	System.out.println(percent++);
		        	bar.setValue(percent);
		        }
		        
				ptsFctDAO.addPoint (mesure);
				
				tampon = br.readLine();
			}
		
			// sauvegarde du global à la fin de la lecture du fichier
			ptsFctDAO.save();
			
		}
		catch (IOException e){
			e.printStackTrace();
			return false;
		} 
		//bar.setEnabled(false);
		return true;
	}
	
	private float InterpolLin(float Vip) {

		double VipIN[] = { -5, 0.22576, 0.67072, 1.04976, 1.4288, 1.7172,
				1.89024, 2.0056, 2.23632, 2.374752, 2.420896, 2.46704,
				2.51923216, 2.55402144, 2.60619712, 2.64100288, 2.78016,
				2.8803584, 3.09328, 3.16163904, 3.35696, 3.4021152, 3.76896,
				4.8072, 10 };
		double LambaOUT[] = { 0.65, 0.65, 0.7, 0.75, 0.8, 0.85, 0.88, 0.9,
				0.95, 0.98, 0.99, 1, 1.03, 1.05, 1.08, 1.1, 1.18, 1.26, 1.43,
				1.5, 1.7, 1.78, 2.43, 5, 5 };

		int i = 1; // indice supérieur dans le tableau
		while (Vip > VipIN[i]) {
			i++;
		}
		double a; // barycentre entre i-1 et 1
		a = ((Vip - VipIN[i - 1]) / (VipIN[i] - VipIN[i - 1])); // pondération
																// de i
		return (float) (LambaOUT[i] * a + LambaOUT[i - 1] * (1 - a));

	}

}
