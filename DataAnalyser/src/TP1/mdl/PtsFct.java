package TP1.mdl;

import java.util.Arrays;

// pts de fonctionnement

public class PtsFct {

	// ID => obligatoire
	private int id;

	// ------------------------ZONE
	// régime min de la zone => obligatoire
	private int nMin;

	// régime max de la zone => obligatoire
	private int nMax;

	// pression collecteur min de la zone => obligatoire
	private float pAirMin;

	// pression collecteur max de la zone => obligatoire
	private float pAirMax;

	// nombre de valeurs mesurées dans la zone
	private int nbTot;

	
	// ------------------------Richesse
	// richesse minimale mesurée dans la zone
	private float rMin;

	// richesse maximale mesurée dans la zone
	private float rMax;

	// richesse moyenne mesurée dans la zone
	private float rMoy;

	// tableau des occurences pour gaussienne de richesse
	private int rGauss[] = new int[21];

	
	// ------------------------Pression Essence
	// pression minimale mesurée dans la zone
	private float pMin;

	// pression maximale mesurée dans la zone
	private float pMax;

	// pression moyenne mesurée dans la zone
	private float pMoy;

	// tableau des occurences pour gaussienne de pression essence
	private int pGauss[] = new int[21];

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getnMin() {
		return nMin;
	}

	public void setnMin(int nMin) {
		this.nMin = nMin;
	}

	public int getnMax() {
		return nMax;
	}

	public void setnMax(int nMax) {
		this.nMax = nMax;
	}

	public float getpAirMin() {
		return pAirMin;
	}

	public void setpAirMin(float pAirMin) {
		this.pAirMin = pAirMin;
	}

	public float getpAirMax() {
		return pAirMax;
	}

	public void setpAirMax(float pAirMax) {
		this.pAirMax = pAirMax;
	}

	public int getNbTot() {
		return nbTot;
	}

	public void setNbTot(int nbTot) {
		this.nbTot = nbTot;
	}

	public float getrMin() {
		return rMin;
	}

	public void setrMin(float rMin) {
		this.rMin = rMin;
	}

	public float getrMax() {
		return rMax;
	}

	public void setrMax(float rMax) {
		this.rMax = rMax;
	}

	public float getrMoy() {
		return rMoy;
	}

	public void setrMoy(float rMoy) {
		this.rMoy = rMoy;
	}

	public int[] getrGauss() {
		return rGauss;
	}

	public void setrGauss(int[] rGauss) {
		this.rGauss = rGauss;
	}

	public float getpMin() {
		return pMin;
	}

	public void setpMin(float pMin) {
		this.pMin = pMin;
	}

	public float getpMax() {
		return pMax;
	}

	public void setpMax(float pMax) {
		this.pMax = pMax;
	}

	public float getpMoy() {
		return pMoy;
	}

	public void setpMoy(float pMoy) {
		this.pMoy = pMoy;
	}

	public int[] getpGauss() {
		return pGauss;
	}

	public void setpGauss(int[] pGauss) {
		this.pGauss = pGauss;
	}

	@Override
	public String toString() {
		return "PtsFct [id=" + id + ", nMin=" + nMin + ", nMax=" + nMax
				+ ", pAirMin=" + pAirMin + ", pAirMax=" + pAirMax + ", nbTot="
				+ nbTot + ", rMin=" + rMin + ", rMax=" + rMax + ", rMoy="
				+ rMoy + ", rGauss=" + Arrays.toString(rGauss) + ", pMin="
				+ pMin + ", pMax=" + pMax + ", pMoy=" + pMoy + ", pGauss="
				+ Arrays.toString(pGauss) + "]";
	}

	public PtsFct(int id, int nMin, int nMax, float pAirMin, float pAirMax,
			int nbTot, float rMin, float rMax, float rMoy, int[] rGauss,
			float pMin, float pMax, float pMoy, int[] pGauss) {
		super();
		this.id = id;
		this.nMin = nMin;
		this.nMax = nMax;
		this.pAirMin = pAirMin;
		this.pAirMax = pAirMax;
		this.nbTot = nbTot;
		this.rMin = rMin;
		this.rMax = rMax;
		this.rMoy = rMoy;
		this.rGauss = rGauss;
		this.pMin = pMin;
		this.pMax = pMax;
		this.pMoy = pMoy;
		this.pGauss = pGauss;
	}

	public PtsFct() {
		// utilisé pour le global qui est enregistré en BDD en dernier
		super();
		//besoin d'avoir 2 tables !
		int tabZeroR[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		int tabZeroP[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		this.id = 0;
		this.nMin = 10000;
		this.nMax = 0;
		this.pAirMin = 1;
		this.pAirMax = 0;
		this.nbTot = 0;
		this.rMin = 2;
		this.rMax = 0;
		this.rMoy = 0;
		this.rGauss = tabZeroR;
		this.pMin = 8;
		this.pMax = 0;
		this.pMoy = 0;
		this.pGauss = tabZeroP;
	}
	
	

}
