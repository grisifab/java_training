package TP1.mdl;

public class ZoneAffichage {

	// régime min de la zone => obligatoire
	private int nMin;

	// régime max de la zone => obligatoire
	private int nMax;

	// pression collecteur min de la zone => obligatoire
	private float pAirMin;

	// pression collecteur max de la zone => obligatoire
	private float pAirMax;
	
	// passage au global, autres valeurs inutiles
	private boolean globalYN;

	
	public int getnMin() {
		return nMin;
	}

	public void setnMin(int nMin) {
		this.nMin = nMin;
	}

	public int getnMax() {
		return nMax;
	}

	public void setnMax(int nMax) {
		this.nMax = nMax;
	}

	public float getpAirMin() {
		return pAirMin;
	}



	public void setpAirMin(float pAirMin) {
		this.pAirMin = pAirMin;
	}

	public float getpAirMax() {
		return pAirMax;
	}

	public void setpAirMax(float pAirMax) {
		this.pAirMax = pAirMax;
	}

	public boolean isGlobalYN() {
		return globalYN;
	}

	public void setGlobalYN(boolean globalYN) {
		this.globalYN = globalYN;
	}

	@Override
	public String toString() {
		return "ZoneAffichage [nMin=" + nMin + ", nMax=" + nMax + ", pAirMin="
				+ pAirMin + ", pAirMax=" + pAirMax + ", globalYN=" + globalYN
				+ "]";
	}

	public ZoneAffichage(int nMin, int nMax, float pAirMin, float pAirMax,boolean globalYN) {
		super();
		this.nMin = nMin;
		this.nMax = nMax-1;
		this.pAirMin = pAirMin;
		this.pAirMax = (float) (Math.min(pAirMax , 1) - 0.01);
		this.globalYN = globalYN;
	}

	public ZoneAffichage() {
		super();
		this.nMin = 0;
		this.nMax = 6000;
		this.pAirMin = 0;
		this.pAirMax = 1;
		this.globalYN = true;
	}

}
