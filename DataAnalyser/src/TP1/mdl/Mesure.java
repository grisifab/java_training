package TP1.mdl;

public class Mesure {
	
	float regime;
	float pressAir;
	float pressEss;
	float richesse;
	
	
	public float getRegime() {
		return regime;
	}
	public void setRegime(float regime) {
		this.regime = regime;
	}
	public float getPressAir() {
		return pressAir;
	}
	public void setPressAir(float pressAir) {
		this.pressAir = pressAir;
	}
	public float getPressEss() {
		return pressEss;
	}
	public void setPressEss(float pressEss) {
		this.pressEss = pressEss;
	}
	public float getRichesse() {
		return richesse;
	}
	public void setRichesse(float richesse) {
		this.richesse = richesse;
	}
	
	@Override
	public String toString() {
		return "Mesure [regime=" + regime + ", pressAir=" + pressAir
				+ ", pressEss=" + pressEss + ", richesse=" + richesse + "]";
	}
	
	public Mesure(float regime, float pressAir, float pressEss, float richesse) {
		super();
		this.regime = regime;
		this.pressAir = pressAir;
		this.pressEss = pressEss;
		this.richesse = richesse;
	}
	
	public Mesure() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
