package TP1.affichage;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import TP1.dao.DAO;
import TP1.fichiers.Lecture;
import TP1.interfaces.Observateur;
import TP1.mdl.PtsFct;
import TP1.mdl.ZoneAffichage;

public class Principal extends JFrame {
	
	private JMenuBar menuBar = new JMenuBar();
	private JMenu test1 = new JMenu("Données");
	private JMenuItem item1 = new JMenuItem("Effacer");
	private JMenuItem item2 = new JMenuItem("Ajouter");
	private JMenuItem item3 = new JMenuItem("Retracer");
	
	private Carto carto1 = new Carto();
	private Gauss gauss2 = new Gauss();
	private Carto carto3 = new Carto();
	private Carto carto4 = new Carto();
	private Data data5 = new Data();
	private JPanel container = new JPanel();
	float result[][];
	int resultGauss[][];
	PtsFct resultData,resultDataG;
	
	public Principal (DAO<PtsFct> ptsFctDAO){
		
		final DAO<PtsFct> comPtsFctDAO = ptsFctDAO;
		
//		ptsFctDAO.init();
//		result = ptsFctDAO.getAll();// recup données moyennes de la BDD
//		if(result == null) System.out.println("quand vide");
//		
//		resultGauss = ptsFctDAO.getGauss();// recup des gaussiennes Richesse et Pression
//		resultDataG = ptsFctDAO.find(0); // recup synthèse dans indice 0
		
		Toolkit outil = getToolkit();
		
		this.data5.addObservateur(new Observateur(){     
			public void update(ZoneAffichage zAff) {
				System.out.println(zAff);
				if(zAff.isGlobalYN()){
					//global
					resultData = comPtsFctDAO.find(0);
					resultGauss = comPtsFctDAO.getGauss();
				} else {
					// zone locale
					// définition des indices extêmes de la zone
					zAff.setnMax(Math.min(zAff.getnMax(),resultDataG.getnMax()-1));
					int indMin = (int) ((Math.floor(zAff.getnMin() / 100) * 20) + Math.floor(zAff.getpAirMin() / 0.05) + 1);
					int indMax = (int) ((Math.floor(zAff.getnMax() / 100) * 20) + Math.floor(zAff.getpAirMax() / 0.05) + 1);
					
					System.out.println(indMin + " ----------- " +indMax);
					resultData = comPtsFctDAO.findX2Y(indMin,indMax);
					resultGauss = comPtsFctDAO.getGaussFrom(resultData);
				}
				gauss2.initVal(resultGauss);
				gauss2.repaint();
				data5.initVal(resultData);
				data5.repaint();
			}
		});
		
		Dimension screenSize = new Dimension(outil.getScreenSize());
		System.out.println("mougoi : " + screenSize);
		Dimension dim1 = new Dimension((screenSize.width-10)*2/3, (screenSize.height-50)*3/5);
		//Dimension dim1 = new Dimension((screenSize.width-10)*2/3, (screenSize.height-40)*3/5);
		Dimension dim2 = new Dimension((screenSize.width-10)/3,dim1.height);
		Dimension dim3 = new Dimension(dim2.width,(screenSize.height-50)*2/5);
		//Dimension dim3 = new Dimension(dim2.width,(screenSize.height-40)*2/5);
		Dimension dim4 = dim3;
		Dimension dim5 = dim3;
		//Insets marge = new Insets(5,5,5,5);
		
		ArrayList <GridBagConstraints> contraintes = new ArrayList <GridBagConstraints>();
		for (int i = 0; i < 5; i++){
			contraintes.add(new GridBagConstraints());
			contraintes.get(i).fill = GridBagConstraints.HORIZONTAL;
			//contraintes.get(i).insets = marge;	
		}
		
		JFrame fenetre = new JFrame();
		fenetre.setVisible(true);
		fenetre.setTitle("Data Analyser");
		fenetre.setSize(screenSize);
		fenetre.setLocationRelativeTo(null);
		fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		container.setLayout(new GridBagLayout());
	 
		
		contraintes.get(0).gridx = 0;
		contraintes.get(0).gridy = 0;
		contraintes.get(0).gridwidth = 2;
//		carto1.initVal(result, 1);
	    carto1.setPreferredSize(dim1);
	    container.add(carto1, contraintes.get(0));
	    
	    contraintes.get(1).gridx = 2;
	    contraintes.get(1).gridy = 0;
	    contraintes.get(1).gridwidth = 1;
//	    gauss2.initVal(resultGauss);
	    gauss2.setPreferredSize(dim2);
	    container.add(gauss2, contraintes.get(1));
	    
	    contraintes.get(2).gridx = 0;
	    contraintes.get(2).gridy = 1;
//	    carto3.initVal(result, 2);
	    carto3.setPreferredSize(dim3);
	    container.add(carto3, contraintes.get(2));
	    
	    contraintes.get(3).gridx = 1;
	    contraintes.get(3).gridy = 1;
//	    carto4.initVal(result, 3);
	    carto4.setPreferredSize(dim4);
	    container.add(carto4, contraintes.get(3));
	    
	    contraintes.get(4).gridx = 2;
	    contraintes.get(4).gridy = 1;
//	    data5.initVal(resultDataG);
	    data5.setPreferredSize(dim5);
	    container.add(data5, contraintes.get(4));
		
	    container.setBackground(new Color(40, 40, 40));
	    
	    fenetre.setContentPane(container);

		item1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Effacer");
				comPtsFctDAO.erase();
			}
		});

		item2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Ajouter");
				//File file = new File("/home/fab/Prog_en_C/Trait_acqui/log_30.csv");
				File file ;
				JFileChooser dialogue = new JFileChooser(new File("."));
				if (dialogue.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					file = dialogue.getSelectedFile();
					Lecture lecture = new Lecture();
					lecture.traiteFichier(file, comPtsFctDAO);
					System.out.println(file.getAbsolutePath());
				}
				
				
			}
		});
		
		item3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Retracer");
				retrace(comPtsFctDAO);
			}
		});
	    
	    this.test1.add(item1);
	    this.test1.add(item2);
	    this.test1.add(item3);
	    this.menuBar.add(test1);
	    
	    retrace(ptsFctDAO);
	    
	    fenetre.setJMenuBar(menuBar);
	    fenetre.setVisible(true);
//	    fenetre.repaint();
	}
	
	private void retrace (DAO<PtsFct> ptsFctDAO){
		ptsFctDAO.init();
		result = ptsFctDAO.getAll();// recup données moyennes de la BDD
		if(result == null) System.out.println("quand vide");
		
		resultGauss = ptsFctDAO.getGauss();// recup des gaussiennes Richesse et Pression
		resultDataG = ptsFctDAO.find(0); // recup synthèse dans indice 0
		
		carto1.initVal(result, 1);
		carto1.repaint();
		
		gauss2.initVal(resultGauss);
		gauss2.repaint();
		
		carto3.initVal(result, 2);
		carto3.repaint();
		
		carto4.initVal(result, 3);
		carto4.repaint();
		
		data5.initVal(resultDataG);
		data5.repaint();
		
	}
	
}
