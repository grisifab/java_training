package TP1.affichage;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

public class Progress extends JFrame {
	
	public Progress(JProgressBar bar){
		
	    this.setSize(400, 40);
	    this.setTitle("Progression Traitement Données");
	    this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    this.setLocationRelativeTo(null);      
	      
	    bar.setMaximum(100);
	    bar.setMinimum(0);
	    bar.setStringPainted(true);      
	    this.getContentPane().add(bar, BorderLayout.CENTER);
	    
	    this.setVisible(true);      
	}

}
