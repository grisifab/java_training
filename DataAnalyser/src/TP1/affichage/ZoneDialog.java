package TP1.affichage;

import javax.swing.JDialog;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Formatter;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import TP1.mdl.ZoneAffichage;

public class ZoneDialog extends JDialog {

	private ZoneAffichage zInfo = new ZoneAffichage();
	private boolean sendData;

	private JLabel nMinLabel, nMaxLabel, pMinLabel, pMaxLabel;
	private JTextField nMinTF, nMaxTF, pMinTF, pMaxTF;

	public ZoneDialog(JFrame parent, String title, boolean modal) {
		super(parent, title, modal);
		this.setSize(550, 200);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.initComponent();
	}

	public ZoneAffichage showZDialog() {
		this.sendData = false;
		this.setVisible(true);
		return(this.sendData)? this.zInfo : null;
	}

	private void initComponent() {

		// régime minimum
		JPanel panNMin = new JPanel();
		panNMin.setBackground(Color.white);
		panNMin.setPreferredSize(new Dimension(220, 60));
		nMinTF = new JTextField();
		;
		nMinTF.setPreferredSize(new Dimension(100, 25));
		panNMin.setBorder(BorderFactory
				.createTitledBorder("Régime moteur (rpm)"));
		nMinLabel = new JLabel("Saisir régime minimal :");
		panNMin.add(nMinLabel);
		panNMin.add(nMinTF);

		// régime maximum
		JPanel panNMax = new JPanel();
		panNMax.setBackground(Color.white);
		panNMax.setPreferredSize(new Dimension(220, 60));
		nMaxTF = new JTextField();
		nMaxTF.setPreferredSize(new Dimension(100, 25));
		panNMax.setBorder(BorderFactory
				.createTitledBorder("Régime moteur (rpm)"));
		nMaxLabel = new JLabel("Saisir régime maximal :");
		panNMax.add(nMaxLabel);
		panNMax.add(nMaxTF);

		// pression air minimum
		JPanel panPMin = new JPanel();
		panPMin.setBackground(Color.white);
		panPMin.setPreferredSize(new Dimension(220, 60));
		pMinTF = new JTextField();
		pMinTF.setPreferredSize(new Dimension(100, 25));
		panPMin.setBorder(BorderFactory
				.createTitledBorder("Pression admission (bar)"));
		pMinLabel = new JLabel("Saisir pression air minimale :");
		panPMin.add(pMinLabel);
		panPMin.add(pMinTF);

		// pression air maximum
		JPanel panPMax = new JPanel();
		panPMax.setBackground(Color.white);
		panPMax.setPreferredSize(new Dimension(220, 60));
		pMaxTF = new JTextField();
		pMaxTF.setPreferredSize(new Dimension(100, 25));
		panPMax.setBorder(BorderFactory
				.createTitledBorder("Pression admission(bar)"));
		pMaxLabel = new JLabel("Saisir pression air maximale :");
		panPMax.add(pMaxLabel);
		panPMax.add(pMaxTF);

		JPanel content = new JPanel();
		content.setBackground(Color.white);
		content.add(panNMin);
		content.add(panNMax);
		content.add(panPMin);
		content.add(panPMax);

		JPanel control = new JPanel();
		JButton okBouton = new JButton("OK");

		okBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					zInfo = new ZoneAffichage(
							Integer.valueOf(nMinTF.getText()), 
							Integer.valueOf(nMaxTF.getText()),
							Float.valueOf(pMinTF.getText()),
							Float.valueOf(pMaxTF.getText()),
							false);
					sendData = true;
					setVisible(false);
				} catch (NumberFormatException ex) {
					System.out.println("mes");
				}
			}
		});

		JButton cancelBouton = new JButton("Annuler");
		cancelBouton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
			}
		});

		control.add(okBouton);
		control.add(cancelBouton);

		this.getContentPane().add(content, BorderLayout.CENTER);
		this.getContentPane().add(control, BorderLayout.SOUTH);
	}
}
