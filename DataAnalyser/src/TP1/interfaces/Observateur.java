package TP1.interfaces;

import TP1.mdl.ZoneAffichage;

public interface Observateur {
	public void update(ZoneAffichage zAff);
}
