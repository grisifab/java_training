package MaCalculette;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Calculatrice extends JFrame {
	private JPanel container = new JPanel();
	private String[] contButt = { "0", "1", "2", "3", "4", "5", "6", "7", "8",
			"9", ".", "C", "+", "-", "*", "/", "=" };
	private JButton[] listButt = new JButton[contButt.length];
	private JLabel ecran = new JLabel("0");
	private Dimension d = new Dimension(20, 20);

	public Calculatrice() {
		this.setTitle("Ma calculette");
		this.setSize(200, 200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		inizializazion();
		this.setContentPane(container);
		this.setVisible(true);
	}

	private void inizializazion() {
		ecran.setPreferredSize(new Dimension(180, 40));
		ecran.setHorizontalAlignment(JLabel.RIGHT);
		container.add(ecran);
		for (int i = 0; i < contButt.length; i++) {
			listButt[i] = new JButton(contButt[i]);
			listButt[i].setSize(d);
			listButt[i].addActionListener(new buttListener());
			container.add(listButt[i]);
		}
	}

	class buttListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String str = ((JButton) e.getSource()).getText();
			int ascci = str.charAt(0);//C67 =61
			switch(ascci)
			{
			case 61 : // =
				ecran.setText(calcul(ecran.getText()));
				break;
			
			case 67 : // C
				ecran.setText("0");
				break;
				
			default :
				if (ecran.getText() != "0"){
					str = ecran.getText() + str;
				}
				ecran.setText(str);
			}
		}
	}
	
	private String calcul(String operation){
		System.out.println(operation);
		double[] chiffres = {0,0,0};
		int[] deci = {0,0};
		int[] nbC = {0,0};
		int[] sens = {1,1};
		char benur,ope='+';
		int etape = 0;
		int ascci;
		//	+	-	*	/	43	45	42	47
		// 0 1...9	48 49...57
		for(int i = 0; i < operation.length(); i++){
			benur = operation.charAt(i);
			ascci = benur;
			System.out.println(ascci);
			switch(ascci)
			{
			case 42 :// *
			case 43 :// +
			case 47 :// /
				ope = benur;
				etape++;
				break;
			
			case 45 :// - ou négatif !
				if(nbC[etape]==0){//négatif
					sens[etape] = -1;
				} else {
					ope = benur;
					etape++;
				}
				break;
				
			case 46://.
				deci[etape] = 1;
				break;
			
			default :
				nbC[etape]++;
				if(deci[etape]==0){//chiffre entier
					chiffres[etape]=chiffres[etape]*10+(double)Character.getNumericValue(benur);
					System.out.println("entier " + etape);
				} else { //décimales
					deci[etape] *=10;
					chiffres[etape]=chiffres[etape]+(double)Character.getNumericValue(benur)/(double)deci[etape];
					System.out.println("deci "+ etape);
				}	
			}
		}
		chiffres[0] = chiffres[0]*(double)sens[0];
		System.out.println(chiffres[0]);
		chiffres[1] = chiffres[1]*(double)sens[1];
		System.out.println(chiffres[1]);
		ascci = ope;
		switch(ascci)
		{
		case 42 ://*
			chiffres[2] = chiffres[0] * chiffres[1];
			break;
		case 43 ://+
			chiffres[2] = chiffres[0] + chiffres[1];
			break;
		case 45 ://- ou négatif
			chiffres[2] = chiffres[0] - chiffres[1];
			break;
		case 47 ://%
			chiffres[2] = chiffres[0] / chiffres[1];
			break;
		}
		
		System.out.println(chiffres[2]);
		
		return(String.valueOf(chiffres[2]));
	}
}
