package exo3;

public class Exo3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Écrivez un programme Java pour trier un tableau numérique et un tableau de chaînes.
		
		// partie tableau numérique
		int[] tabIn = {12,24,56,11,14,12,0,4,9,10};
		int[] tabOut = new int[tabIn.length];
		final int maxInt = 2147483647;
		int minActu;
		int ind = 0;
		
		for(int i = 0; i < tabOut.length; i++){
			minActu = maxInt;
			for(int j = 0; j < tabIn.length; j++){
				if (tabIn[j] < minActu){
					ind = j;
					minActu = tabIn[j];
				}
			}
			tabOut[i] = minActu;
			tabIn[ind] = maxInt;
			System.out.println(i + "_" + tabOut[i]);
		}
		
///////// partie tableau de chaînes /////////////////////////
		
		String[] strIn = {"ABC","abd","bbb","aaa","wc","moi","ab","","a","0"};
		String[] strOut = new String[strIn.length];
		String maxStr = "zzzzzzzzzzz";
		String minActuS;
		int indS = 0;
		int indC = 0;
		boolean finDeChaine = false;
		
		// tout passer en minuscule
		for(int i = 0; i < strIn.length; i++){
			strIn[i] = strIn[i].toLowerCase();
		}
		
		for(int i = 0; i < strOut.length; i++){
			minActuS = maxStr;
			for(int j = 0; j < strIn.length; j++){
				indC = 0;
				// on teste si une des chaines n'est pas vide avant de faire le while
				finDeChaine = (strIn[j].length() == 0 || minActuS.length() == 0) ? true : false;
				// tant que les caractères sont identiques on progresse ds la chaine (si assez longue)
				while ((!finDeChaine) && (strIn[j].charAt(indC) == minActuS.charAt(indC)))
				{
					finDeChaine = ((indC + 1) == strIn[j].length()) || ((indC + 1) == minActuS.length());
					if (!finDeChaine){indC ++;}
				}
				
				if (!finDeChaine) { // on est sorti car caractère différent
					if (strIn[j].charAt(indC) < minActuS.charAt(indC)){ //et on trouve un caractère plus petit que le min actuel
						minActuS = strIn[j];
						indS = j;
					}
				} else if (strIn[j].length() < minActuS.length()){ // on est sorti sur fin de chaine et string plus petit que le min actu
					minActuS = strIn[j];
					indS = j;
				}
			}
			strOut[i] = minActuS;
			strIn[indS] = maxStr;
			System.out.println(i + "_" + strOut[i]);
		}
	}
}
